<?php

namespace App\Http\Controllers;

use App\Email;
use App\Message;
use App\Partner;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Post;

class SiteController extends Controller
{
    public function index()
    {
        $mainSliders = Slider::orderBy('order','asc')->get();

        $latest = Post::where('featured', 0)
            ->published()
            ->orderBy('id', 'desc')
            ->take(3)
            ->get([
                'id', 'title', 'slug', 'image','excerpt'
            ]);

        $partners = Partner::orderBy('order','asc')->get();

        return view('index', compact('partners', 'mainSliders', 'latest'));
    }


    public function posts()
    {

        $posts = Post::published()->orderBy('id', 'desc')->paginate(20);

        return view('posts', compact('posts'));
    }

    public function page($slug)
    {
        $page = Page::active()->where('slug', $slug)->first();

        return view('page', compact('page'));
    }

    public function post($slug)
    {
        $post = Post::where('slug', $slug)->first();

        return view('post', compact('post'));
    }

    public function partner($id)
    {
        $partner = Partner::where('id', $id)->first();

        return view('partner', compact('partner'));
    }

    public function contactUs()
    {

        return view('contact-us');
    }

    public function message(Request $request)
    {

        $captcha=$_POST['g-recaptcha-response'] ?? null;

        if(!$captcha){
            return back()->with('error', 'Captcha seçilməyib')->withInput();
        }
        $secretKey = "6LepIeAZAAAAAOhElIGWROW_BrGfTHgAM8JpWw_2";
        $ip = $_SERVER['REMOTE_ADDR'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
        // should return JSON with success as true
        if($responseKeys["success"]) {
            $validate = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validate->fails()) {
                return back()->withErrors($validate->errors());
            }

            $data = $request->only('name', 'email', 'subject', 'content','phone');

            $message = Message::create($data);

            if ($message) {
                return back()->with('success', 'Müraciət göndərildi');
            }
        } else {
            echo '<h2>You are spammer ! Get the @$%K out</h2>';
            return false;
        }

        return back()->with('error', 'Xəta baş verdi')->withInput();
    }

    public function subscribe(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validate->fails()) {
            return back()->withErrors($validate->errors());
        }

        $data = $request->only('email');

        $email = Email::firstOrCreate($data);

        if ($email) {
            return back()->with('success', 'Abunə oldunuz. Təşəkkür edirik');
        }

        return back()->with('error', 'Xəta baş verdi');
    }

}
