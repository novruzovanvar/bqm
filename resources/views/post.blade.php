@extends('layout.master')

@section('meta')
    @include('layout.base.meta',[
        'title' => $post->seo_title,
        'description' => $post->meta_description,
        'image' => $post->image
    ])

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fa4e64fed8380c6"></script>
@stop

@section('content')

    <section class="w3l-about-breadcrum">
        <div class="breadcrum-bg py-sm-5 py-4">
            <div class="container py-lg-3 py-2">
                <h2>{{$post->title}}</h2>
            </div>
        </div>
    </section>

    <!-- content-with-photo4 block -->
    <section class="w3l-content-with-photo-4" id="about">
        <div id="content-with-photo4-block" class="pt-5">
            <div class="container py-md-3">
                <div class="about-content">
                    <h2 class="title mb-20">{{$post->excerpt}}</h2>
                {!! $post->body !!}

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_inline_share_toolbox mt-5"></div>
                </div>
            </div>
        </div>
    </section>
@stop
