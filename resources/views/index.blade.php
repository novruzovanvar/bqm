@extends('layout.master')
@section('meta')
    @include('layout.base.meta')
@stop
@section('content')

    @include('layout.sections.slider',['sliders' => $mainSliders])

    <!-- features-4 block -->
    <section class="w3l-index1" id="about">
        <div class="calltoaction-20  py-5 editContent">
            <div class="container py-md-3">

                <div class="calltoaction-20-content row">
                    <div class="column center-align-self col-lg-6 pr-lg-5">
                        <h5 class="editContent">Biz kimik</h5>
                        {!! setting('site.about') !!}
                        <a class="btn btn-secondary btn-theme2 mt-2 mt-lg-3"
                           href="{{route('page',['slug' => 'about'])}}"> Ətraflı</a>
                    </div>
                    <div class="column ccont-left col-lg-6 mt-lg-0 mt-5">
                        <img src="{{Voyager::image(setting('site.about_image'))}}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- features-4 block -->
    <!--  services section -->
    <section class="w3l-index6">
        <div class="features-with-17_sur py-5">
            <div class="container py-lg-5">
                <div class="features-with-17-top_sur">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 mt-md-0 mt-4">
                            <div class="features-with-17-right-tp_sur">
                                <div class="features-with-17-left1 mb-3">
                                    <span class="fa fa-lightbulb-o" aria-hidden="true"></span>
                                </div>
                                <div class="features-with-17-left2">
                                    <h6><a href="#">
                                            <bold>Operativ</bold>
                                            iş</a></h6>
                                    <p> Biz sifarişlərin alınıb işlənməsinin və aparılmış əqdlərə dair hesabatların
                                        təqdim edilməsinin yüksək sürətini təmin edirik.</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mt-md-0 mt-5">
                            <div class="features-with-17-right-tp_sur">
                                <div class="features-with-17-left1 mb-3">
                                    <span class="fa fa-recycle" aria-hidden="true"></span>
                                </div>
                                <div class="features-with-17-left2">
                                    <h6><a href="#url">
                                            <bold>Komanda</bold>
                                            işi</a></h6>
                                    <p>Şirkətimizdə yalnız müxtəlif fəaliyyət sahələrində əməli iş təcrübəsinə malik
                                        mütəxəssislər çalışır.</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
                            <div class="features-with-17-right-tp_sur">
                                <div class="features-with-17-left1 mb-3">
                                    <span class="fa fa-bar-chart" aria-hidden="true"></span>
                                </div>
                                <div class="features-with-17-left2">
                                    <h6><a href="#url">
                                            <bold>Yüksək</bold>
                                            peşəkarlıq</a></h6>
                                    <p> Komandamızın başlıca məqsədi hər bir müştərinin daxili inkişafını <br/> təmin
                                        etməkdir.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  //services section -->
    {{--<section class="w3l-feature-3" id="stats">
        <div class="grid top-bottom">
            <div class="container">
                <div class="heading text-center mx-auto">
                    <h3 class="head text-white">We’re Constantly Improving Our Skills</h3>
                    <p class="my-3 head text-white"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                        posuere cubilia Curae;
                        Nulla mollis dapibus nunc, ut rhoncus
                        turpis sodales quis. Integer sit amet mattis quam.</p>
                </div>
                <div class="middle-section grid-column mt-5 pt-3">
                    <div class="three-grids-columns">
                        <h5>2450 <span>+</span></h5>
                        <h4>Team Members</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.Vestibulum ante ipsum primis in faucibus
                            orci luctus </p>
                    </div>
                    <div class="three-grids-columns">
                        <h5>550 <span>+</span></h5>
                        <h4>Projects</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.Vestibulum ante ipsum primis in faucibus
                            orci luctus </p>
                    </div>
                    <div class="three-grids-columns">
                        <h5>3450 <span>+</span></h5>
                        <h4>Happy Clients</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.Vestibulum ante ipsum primis in faucibus
                            orci luctus </p>
                    </div>
                    <div class="three-grids-columns">
                        <h5>450 <span>+</span></h5>
                        <h4>Media Managment</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.Vestibulum ante ipsum primis in faucibus
                            orci luctus </p>
                    </div>
                    <div class="three-grids-columns">
                        <h5>180 <span>+</span></h5>
                        <h4>Finance</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.Vestibulum ante ipsum primis in faucibus
                            orci luctus </p>
                    </div>
                    <div class="three-grids-columns">
                        <h5>1200 <span>+</span></h5>
                        <h4>Brands</h4>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.Vestibulum ante ipsum primis in faucibus
                            orci luctus </p>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    {{--<section class="w3l-features-4">
        <!-- /features -->
        <div class="features py-5" id="features">
            <div class="container py-md-3">
                <div class="heading text-center mx-auto">
                    <h3 class="head">Advance Features!</h3>
                    <p class="my-3 head"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                        Curae;
                        Nulla mollis dapibus nunc, ut rhoncus
                        turpis sodales quis. Integer sit amet mattis quam.</p>
                </div>
                <div class="fea-gd-vv row mt-5 pt-3">
                    <div class="float-lt feature-gd col-md-6">
                        <div class="icon"><span class="fa fa-handshake-o" aria-hidden="true"></span></div>
                        <div class="icon-info">
                            <h5><a href="#">User Friendly
                                </a></h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                primis in faucibus orci luctus et ultrices posuere primis in faucibus </p>
                            <a href="#" class="red mt-3">Read More <span class="fa fa-angle-right pl-1"></span></a>
                        </div>

                    </div>
                    <div class="float-mid feature-gd col-md-6 mt-md-0 mt-5">
                        <div class="icon"><span class="fa fa-mobile" aria-hidden="true"></span></div>
                        <div class="icon-info">
                            <h5><a href="#">Responsive Layout
                                </a></h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                primis in faucibus orci luctus et ultrices posuere primis in faucibus </p>
                            <a href="#" class="red mt-3">Read More <span class="fa fa-angle-right pl-1"></span></a>
                        </div>
                    </div>
                    <div class="float-rt feature-gd col-md-6 mt-5">
                        <div class="icon"><span class="fa fa-gg" aria-hidden="true"></span></div>
                        <div class="icon-info">
                            <h5><a href="#">Easy to customize</a></h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                primis in faucibus orci luctus et ultrices posuere primis in faucibus </p>
                            <a href="#" class="red mt-3">Read More <span class="fa fa-angle-right pl-1"></span></a>
                        </div>
                    </div>
                    <div class="float-lt feature-gd col-md-6 mt-5">
                        <div class="icon"><span class="fa fa-eye" aria-hidden="true"></span></div>
                        <div class="icon-info">
                            <h5><a href="#">Cross Browser</a>
                            </h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                primis in faucibus orci luctus et ultrices posuere primis in faucibus </p>
                            <a href="#" class="red mt-3">Read More <span class="fa fa-angle-right pl-1"></span></a>
                        </div>

                    </div>
                    <div class="float-mid feature-gd col-md-6 mt-5">
                        <div class="icon"><span class="fa fa-cog" aria-hidden="true"></span></div>
                        <div class="icon-info">
                            <h5><a href="#">
                                    W3C Standard</a>
                            </h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                primis in faucibus orci luctus et ultrices posuere primis in faucibus </p>
                            <a href="#" class="red mt-3">Read More <span class="fa fa-angle-right pl-1"></span></a>
                        </div>
                    </div>
                    <div class="float-rt feature-gd col-md-6 mt-5">
                        <div class="icon"><span class="fa fa-paint-brush" aria-hidden="true"></span></div>
                        <div class="icon-info">
                            <h5><a href="#">Color Option</a>
                            </h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt
                                primis in faucibus orci luctus et ultrices posuere primis in faucibus </p>
                            <a href="#" class="red mt-3">Read More <span class="fa fa-angle-right pl-1"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //features -->
    </section>--}}
    {{--<section class="w3l-feature-2" id="process">
        <div class="grid top-bottom py-5">
            <div class="container py-md-5">
                <div class="heading text-center mx-auto">
                    <h3 class="head text-white">Four Steps for Process</h3>
                    <p class="my-3 head text-white"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                        posuere cubilia Curae;
                        Nulla mollis dapibus nunc, ut rhoncus
                        turpis sodales quis. Integer sit amet mattis quam.</p>

                </div>
                <div class="middle-section row mt-5 pt-3 text-center">
                    <div class="three-grids-columns col-lg-3 col-sm-6 ">
                        <div class="icon"><span class="">1</span></div>
                        <h4>Register</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor.</p>

                    </div>
                    <div class="three-grids-columns col-lg-3 col-sm-6 mt-sm-0 mt-5">
                        <div class="icon"><span class="">2</span></div>
                        <h4>App Download</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor.</p>

                    </div>
                    <div class="three-grids-columns col-lg-3 col-sm-6 mt-lg-0 mt-5">
                        <div class="icon"><span class="">3</span></div>
                        <h4>Product Testing</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor.</p>

                    </div>
                    <div class="three-grids-columns col-lg-3 col-sm-6 mt-lg-0 mt-5">
                        <div class="icon"><span class="">4</span></div>
                        <h4>Pro Support</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor.</p>

                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    <section class="w3l-news" id="news">
        <section id="grids5-block" class="">
            <div class="container py-lg-3">

                <div class="grid-view">
                    <div class="row">
                        @foreach($latest as $post)
                            <div class="col-lg-4 col-md-6 mt-md-4 mt-4">
                                <div class="grids5-info">
                                    <a href="#" class="d-block zoom">
                                        <img src="{{Voyager::image($post->thumbnail('cropped'))}}" alt=""
                                                                          class="img-fluid news-image"></a>
                                    <div class="blog-info">

                                        <h4><a href="#">{!! $post->title !!}</a></h4>

                                        <p class="blog-text">{!! $post->excerpt !!}.</p>
                                        <a href="{{route('post',['slug'=> $post->slug])}}" class="btn btn-news">Ətraflı <span
                                                class="fa fa-angle-right pl-1"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </section>

@endsection
