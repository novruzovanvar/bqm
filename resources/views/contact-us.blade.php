@extends('layout.master')

@section('meta')
    @include('layout.base.meta')
    <script src="https://www.google.com/recaptcha/api.js?hl=az_AZ" async defer></script>

@stop

@section('content')

    <section class="w3l-about-breadcrum">
        <div class="breadcrum-bg py-sm-5 py-4">
            <div class="container py-lg-3 py-2">
                <h2>Əlaqə</h2>
            </div>
        </div>
    </section>
    <!-- content-with-photo4 block -->

    <!-- contact -->
    <section class="w3l-contacts-12" id="contact">
        <div class="contact-top">

            <!-- map -->
            <div class="map">
                {!! setting('site.map')  !!}
            </div>
            <!-- //map -->
        </div>
    </section>
    <!-- //contact -->
    <!-- contact form -->
    <section class="w3l-contacts-2">
        <div class="contacts-main">

            <div class="form-41-mian py-5">
                <div class="container py-md-3">
                    <h3 class="cont-head">Bizə yazın</h3>
                    <div class="d-grid align-form-map">
                        <div class="form-inner-cont">

                            <form action="" method="post" class="main-input">
                                <div class="top-inputs d-grid">
                                    <input type="text" placeholder="Ad Soyad" name="w3lName" id="w3lName" required="">
                                    <input type="email" name="email" placeholder="E-poçt" id="w3lSender" required="">
                                </div>
                                <input type="text" placeholder="Mobil" name="w3lName" id="w3lName" required="">
                                <textarea placeholder="Mesaj" name="w3lMessage" id="w3lMessage" required=""></textarea>
                                <div class="g-recaptcha" data-sitekey="6LepIeAZAAAAALihyBEDMGePY3WYhGxQNVKyt388"></div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-theme3">Göndər</button>
                                </div>
                            </form>
                        </div>

                        <div class="contact-right">
                            <img src="/storage/posts/November2020/hBPYToaEVeGkyjM6nH6C.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="contant11-top-bg py-5">
                <div class="container py-md-3">
                    <div class="d-grid contact section-gap">
                        <div class="contact-info-left d-grid text-center">
                            <div class="contact-info">
                                <span class="fa fa-location-arrow" aria-hidden="true"></span>
                                <h4>Ünvan</h4>
                                <p>{{setting('site.address')}}</p>
                            </div>
                            <div class="contact-info">
                                <span class="fa fa-phone" aria-hidden="true"></span>
                                <h4>Telefon</h4>
                                <p><a href="#">{{setting('site.phone')}}</a></p>
                            </div>
                            <div class="contact-info">
                                <span class="fa fa-envelope-open-o" aria-hidden="true"></span>
                                <h4>E-poçt</h4>
                                <p><a href="#">{{setting('site.email')}}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>

@stop
