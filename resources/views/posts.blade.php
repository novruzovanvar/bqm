@extends('layout.master')

@section('content')

    <section class="w3l-about-breadcrum">
        <div class="breadcrum-bg py-sm-5 py-4">
            <div class="container py-lg-3 py-2">
                <h2>Xəbərlər</h2>
            </div>
        </div>
    </section>

    <section class="w3l-news" id="news">
        <section id="grids5-block" class="">
            <div class="container py-lg-3">

                <div class="grid-view">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-lg-4 col-md-6 mt-md-4 mt-4">
                                <div class="grids5-info">
                                    <a href="#" class="d-block zoom">
                                        <img src="{{Voyager::image($post->thumbnail('cropped'))}}" alt=""
                                             class="img-fluid news-image"></a>
                                    <div class="blog-info">

                                        <h4><a href="#">{!! $post->title !!}</a></h4>

                                        <p class="blog-text">{!! $post->excerpt !!}.</p>
                                        <a href="{{route('post',['slug'=> $post->slug])}}" class="btn btn-news">Ətraflı <span
                                                class="fa fa-angle-right pl-1"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </section>
    </section>
@stop
