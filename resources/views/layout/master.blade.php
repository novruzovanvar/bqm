<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">
    @yield('meta')
    @include('layout.base.css')

    @stack('css')
</head>
<body>


@include('layout.sections.header')

@yield('content')

@include('layout.sections.footer')


@include('layout.base.js')

@stack('js')

</body>

</html>
<!-- // grids block 5 -->
