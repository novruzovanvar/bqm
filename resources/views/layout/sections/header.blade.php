<!-- Top Menu 1 -->
<section class="w3l-top-menu-1">
    <div class="top-hd">
        <div class="container">
            <header class="row">
                <div class="social-top col-sm-6 col-6 pl-0">
                    <li>Email: {{setting('site.email')}}</li>

                </div>
                <div class="accounts col-sm-6 col-6 pr-0">
                    <div class="main-social-header">
                        <a href="{{setting('site.facebook')}}" class="facebook"><span class="fa fa-facebook"></span></a>
                        <a href="{{setting('site.twitter')}}" class="twitter"><span class="fa fa-twitter"></span></a>
                        <a href="{{setting('site.instagram')}}" class="instagram"><span class="fa fa-instagram"></span></a>
                        <a href="{{setting('site.youtube')}}" class="google-plus"><span class="fa fa-youtube"></span></a>
                    </div>
                </div>

            </header>
        </div>
    </div>
</section>
<!-- //Top Menu 1 -->
<section class="w3l-bootstrap-header">
    <nav class="navbar navbar-expand-lg navbar-light py-lg-2 py-2">
        <div class="container">
            <a class="navbar-brand"  href="/">{!!  setting('site.title') !!}</a>
            <!-- if logo is image enable this
          <a class="navbar-brand" href="#index.html">
              <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
          </a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon fa fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse pull-right" id="navbarSupportedContent">

                {!! str_replace(['navbar-nav','<li','<a','caret'],['navbar-nav mx-auto','<li class="nav-item"','<a class="nav-link"','fa fa-chevron-down'],menu('site','bootstrap'))!!}

                {{--<p>Əlaqə<span class="fa fa-headphones pl-1"></span><br>
                    <a href="tel:{{setting('site.phone')}}">{{setting('site.main_phone')}}</a>
                </p>--}}
            </div>
        </div>
    </nav>
</section>
