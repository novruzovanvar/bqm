<!-- grids block 5 -->
<section class="w3l-footer-29-main">
    <div class="footer-29">
        <div class="container">
            <div class="d-grid grid-col-4 footer-top-29">
                <div class="footer-list-29 footer-1">
                    <h6 class="footer-title-29">Əlaqə</h6>
                    <ul>
                        <li>
                            <p><span class="fa fa-map-marker"></span>
                            {{setting('site.address')}}
                            </p>
                        </li>
                        <li>
                            <a href="tel:{{setting('site.main_phone')}}">
                                <span class="fa fa-phone"></span>
                                {{setting('site.main_phone')}}
                            </a>
                        </li>
                        <li><a href="mailto:{{setting('site.email')}}" class="mail"><span
                                    class="fa fa-envelope-open-o"></span>{{setting('site.email')}}</a></li>
                    </ul>
                    <div class="main-social-footer-29">
                        <a href="{{setting('site.facebook')}}" class="facebook"><span class="fa fa-facebook"></span></a>
                        <a href="{{setting('site.twitter')}}" class="twitter"><span class="fa fa-twitter"></span></a>
                        <a href="{{setting('site.instagram')}}" class="instagram"><span class="fa fa-instagram"></span></a>
                        <a href="{{setting('site.youtube')}}" class="google-plus"><span class="fa fa-youtube"></span></a>
                    </div>
                </div>
                <div class="footer-list-29 footer-2">
                    <h6 class="footer-title-29">Linklər</h6>
                    {!! menu('site') !!}
                </div>
                <div class="footer-list-29 footer-3">

                    <h6 class="footer-title-29">Məlumat </h6>
                    <form action="{{route('subscribe')}}#footer"  class="subscribe" method="post">
                        <input type="email" name="email" placeholder="Email" required="">
                        @csrf
                        <button><span class="fa fa-envelope-o"></span></button>
                    </form>
                    @if(session('success'))
                        <div class="pl-2 text-success">{{ session('success') }}</div>
                    @endif

                    @if(session('error'))
                        <div class="pl-2 text-danger">{{ session('error') }}</div>
                    @endif
                    <p>Yeniliklərdən xəbərdar olmaq üçün abunə ol</p>

                </div>
                <div class="footer-list-29 footer-4">
                    <ul>
                        <h6 class="footer-title-29">İnformasiya</h6>
                        <li><a href="/page/faq">Ən çox verilən suallar</a></li>
                        <li><a href="/page/faydali-kecidler">Faydalı linklər</a></li>
                    </ul>
                </div>
            </div>
            <div class="d-grid grid-col-2 bottom-copies">
                <p class="copy-footer-29">© {{ date('Y') }} {{strip_tags(setting('site.title'))}}</p>

            </div>
        </div>
    </div>
    <!-- move top -->
    <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fa fa-angle-up"></span>
    </button>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
    <!-- /move top -->
</section>
