<title>{{ strip_tags($title ?? setting('site.title'))}}</title>
<meta name="description" content="{{$description ?? setting('site.description')}}" />
<meta name="title" content="{{$title ?? setting('site.title')}}">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="{{request()->getUri()}}">
<meta property="og:title" content="{{$title ?? setting('site.title')}}">
<meta property="og:description" content="{{$description ?? setting('site.description')}}">
<meta property="og:image" content="{{Voyager::image($image ?? setting('site.logo'))}}">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="{{request()->getUri()}}">
<meta property="twitter:title" content="{{$title ?? setting('site.title')}}">
<meta property="twitter:description" content="{{$description ?? setting('site.description')}}">
<meta property="twitter:image" content="{{Voyager::image($image ?? setting('site.logo'))}}">

<meta name="googlebot" content="noindex, nofollow">
<meta name="robots" content="noindex, nofollow">
