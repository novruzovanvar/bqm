<!-- web fonts -->
<link href="//fonts.googleapis.com/css?family=Poppins:100,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
<!-- //web fonts -->
<!-- Template CSS -->
<link rel="stylesheet" href="/template/assets/css/style-starter.css">
<style>
    body{
        font-size: 18px;
    }
    .main-social-header a{
        width: 35px;
        height: 35px;
        text-align: center;
        line-height: 35px;
        display: inline-block;
    }

    .w3l-bootstrap-header a.navbar-brand{
        font-size: 20px !important;
        border-right: 1px solid #ccc;
        margin-left: -15px;
        padding-right: 15px;
    }

    .navbar-brand span{
        font-size: 20px !important;
    }

    .nav-item{
        position: relative;
    }
    .footer-list-29.footer-2 ul li > ul {
        display: none;
    }
</style>
