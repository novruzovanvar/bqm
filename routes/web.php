<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\SiteController::class,'index']);
Route::get('/partners/{id}',[\App\Http\Controllers\SiteController::class,'partner'])->name('partner');

Route::get('/page/{slug}',[\App\Http\Controllers\SiteController::class,'page'])->name('page');
Route::get('/post/{slug}',[\App\Http\Controllers\SiteController::class,'post'])->name('post');
Route::get('/posts',[\App\Http\Controllers\SiteController::class,'posts'])->name('posts');
Route::get('/contact-us',[\App\Http\Controllers\SiteController::class,'contactUs'])->name('contact');
Route::post('/message',[\App\Http\Controllers\SiteController::class,'message'])->name('message');
Route::post('/subscribe',[\App\Http\Controllers\SiteController::class,'subscribe'])->name('subscribe');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    //Asset Routes
    Route::get('voyager-assets', [\App\Http\Controllers\Controller::class,'assets'])->name('voyager.voyager_assets');

});
